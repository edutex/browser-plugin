let useridElements = document.getElementsByClassName("moodle-userid");

if (useridElements.length > 0) {
  const moodleUserId = useridElements[0].dataset.userid;
  console.log("Parsed userid: ", moodleUserId);
  chrome.runtime.sendMessage({
    moodleUserId,
    type: "moodleUserIdRead",
  });
}
else {
  console.log("Could not parse userid because no element with class moodle-userid");
}
