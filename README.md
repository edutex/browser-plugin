# Edutex Website Logging Browser Plugin

This is the browser plugin is part of the edutex infrastructure.
It is used to log the browser events as long as a learner is active in the moodle learning management system (LMS).
The plugin reads the moodle user id automatically from the associated edutex moodle plugin.

## Modifying the Manifest

Modifying the manifest.json is absolutely required in order for this plugin to work in your environment. 

### Modifying the manifest for different browsers

The manifest in this repository is for the chrome version of the browser plugin. For Firefox and Safari, there need to be several modifications, which can be found at various places on the internet.

### Modifying the URLs in 'content_scripts'

The URLs in the "matches" setting are those where this plugin is allowed to inject its code and which it uses to determine whether it should start logging the events.
This should be the URLs where your moodle LMS is running. 
For example, if your moodle LMS is running at https://lms.yourinstitution.example/ then the URLs should be:
https://lms.yourinstitution.example/*
Or, if you want to be on the safe side, you can use wildcards
https://*.yourinstitution.example/*


## Modifying the Settings

In the settings, the following settings MUST be overwritten:

### START_DOMAINS
These are the domains which are used to start the tracking. This should mostly match the "matches" from the manifest.json file, except that wildcard strings are not allowed here due to the way that the check is implemented, which is a simple string.includes() call. Thus, you can get away with using only part of your domain, for example "lms.yourinstitution.example". 

### TRACKING_URL
This is the URL where the tracking data is sent. This should be the same URL as the one used by the edutex moodle plugin.

### TRACKING_USERNAME and TRACKING_PASSWORD
If you are using a username and password to authenticate to the tracking server, you have to set these here.