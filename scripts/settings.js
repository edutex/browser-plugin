var Settings = {
  START_DOMAINS: ["moodle.impact.studiumdigitale.uni-frankfurt.de", "difa.edutec.science"],
  TRACKING_DURATION_MS: 600000,
  TRACKING_URL: "https://tracking.difa.edutec.science/datums",
  TRACKING_USERNAME: "user",
  TRACKING_PASSWORD: "Yjzfyws7Lh5HjT",

  KEYS: {
    isFirstLaunch: "isFirstLaunch",
    extensionUserId: "extensionUserId",
    moodleUserId: "moodleUserId",
  },

  load: function(onLoadedCallback) {   
    chrome.storage.sync.get(
      [this.KEYS.isFirstLaunch,
        this.KEYS.extensionUserId,
        this.KEYS.moodleUserId],
      (items) => {
        this[this.KEYS.isFirstLaunch] = items[this.KEYS.isFirstLaunch] ? items[this.KEYS.isFirstLaunch] : true;
        this[this.KEYS.extensionUserId] = items[this.KEYS.extensionUserId] ? items[this.KEYS.extensionUserId] : null;
        this[this.KEYS.moodleUserId] = items[this.KEYS.moodleUserId] ? items[this.KEYS.moodleUserId] : null;
        console.log("Loaded Settings: ", this);
        onLoadedCallback();
      }
    );
  },

  saveSetting: function(key, value) {
    let setting = {};
    setting[key] = value;
    chrome.storage.sync.set(setting, () => {
      console.log("Saved Setting: ", setting);
      this[key] = value;
    });
  }

}

