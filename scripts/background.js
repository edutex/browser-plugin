let active = false;
let intervalHandle = null;

init();

chrome.tabs.onUpdated.addListener((tabid, changedInfo, tab) => {
  if (changedInfo.status === "complete") {
    doTabTracking(tab, "navigation");
  }
});

chrome.tabs.onActivated.addListener(function (activeInfo) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    // since only one tab should be active and in the current window at once
    // the return variable should only have one entry
    var activeTab = tabs[0];
    doTabTracking(activeTab, "navigation");
  });
});

function init() {
  Settings.load(onSettingsLoaded);
}

function start() {
  active = true;
  chrome.action.setBadgeText({ text: "o" });
  chrome.action.setBadgeBackgroundColor({ color: "green" });
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.type == "moodleUserIdRead") {
    console.log(request);
    Settings.saveSetting(Settings.KEYS.moodleUserId, request.moodleUserId);
  }

  sendResponse();
});

function onSettingsLoaded() {
  if (!Settings.extensionUserId) {
    const extensionUserId = "_" + Math.random().toString(36).substr(2, 14);
    Settings.saveSetting("extensionUserId", extensionUserId);
    Settings.saveSetting("isFirstLaunch", true);
  }
}


function stop() {
  active = false;
  chrome.action.setBadgeText({ text: "-" });
  chrome.action.setBadgeBackgroundColor({ color: "red" });
}

function isStartDomain(domain) {
  for (let i = 0; i < Settings.START_DOMAINS.length; i++) {
    if (domain.includes(Settings.START_DOMAINS[i])) {
      return true;
    }
  }
  return false;
}


function getDomainFromURL(url) {
  try {
    url = new URL(url);
  } catch(e) {
    console.log("Error when trying to create URL from ", url);
    return null; 
  } 
  return url.hostname.replace("www.", '').replace("http://", "").replace("https://", "").toLowerCase();
}

function getStrippedURL(url) {
  url = new URL(url);
  return url.origin;
}


function doTabTracking(tab, type) {
  if (tab.url === "") return;
  const domain = getDomainFromURL(tab.url);

  if (isStartDomain(domain)) {
    startNavigationTracking();
  } else {
    initiateStopTrackingCountdown();
  }

  if (active) {
    if (!isStartDomain(domain)) {
      let strippedURL = getStrippedURL(tab.url);
      postNavigationEvent(domain, strippedURL, type);
    }
  }
}

function startNavigationTracking() {
  const duration = Settings.TRACKING_DURATION_MS;
  console.log(
    "Starting the tracking because moodle was visited.",
    "Tracking will be active for: ",
    duration
  );
  start();
  if (intervalHandle) {
    clearInterval(intervalHandle);
    intervalHandle = null;
  }
}

function initiateStopTrackingCountdown() {
  // If we are already in a stop tracking countdown, we do not re-initiate it
  if (intervalHandle) {
    return;
  }

  const duration = Settings.TRACKING_DURATION_MS;
  console.log("Stopping the tracking in: ", duration);

  intervalHandle = setInterval(() => {
    stop();
    console.log("Stopping the tracking because the time ran out");
  }, duration);
}

function toggleActive() {
  active = !active;
  if (active) start();
  else stop();
}


function postNavigationEvent(domain, url, type) {
  let domainToLower = domain.toLowerCase();

  let payload =
  {
    userid: Settings.moodleUserId,
    extensionUserid: Settings.extensionUserId,
    url,
    domain: domainToLower
  }

  const data = {
    name: type,
    timestamp: Date.now(),
    payload
  };

  postData(data);
}


function postData(data) {
  const url = Settings.TRACKING_URL;
  const credentials = btoa(`${Settings.TRACKING_USERNAME}:${Settings.TRACKING_PASSWORD}`);
  console.log("Sending Data:", data);

  const auth = "Basic " + credentials;
  const response = fetch(url, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      "Authorization": auth
    },
    body: JSON.stringify(data),
  })
    .then(function (resp) {
      return resp;
    })
    .catch((error) => console.log(error));
  return response;
}